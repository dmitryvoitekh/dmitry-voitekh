#ifndef SPECIAL_ITEM_H
#define SPECIAL_ITEM_H
#include "item.h"
#include <string>
#include <iostream>

const int NUM3=4;

const QString pay_kind[NUM3]={"electricity", "water", "gas", "taxes"};

class special_item: public item
{
public:
    special_item(int c);
    void operator=(item & i);
};

#endif // SPECIAL_ITEM_H
