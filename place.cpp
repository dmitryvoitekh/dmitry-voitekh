#include "place.h"
#include <iostream>
#include <iomanip>

using namespace std;

void show_places()
{
    cout << endl;
    cout << "-------------------------------------------\n";
    for(int i=0; i<NUM1/2; i++)
    //cout << i+1 << "." << setw(16)<< places_list[i] << "  " << NUM1/2+i+1 << "." << setw(13) << places_list[NUM1/2+i] << endl;
    cout << "-------------------------------------------\n";
}

place::place()
{
    name="Unknown";
    kind=0;
}

place::place(QString n, int k):name(n),kind(k)
{
}

place::~place()
{
}

void place::reset(QString n, int k)
{
    name=n;
    kind=k;
}

ostream & operator<<(ostream & os, const place & p)
{
    os << places_list[p.kind] << " \"" << p.name << "\"";
    return os;
}