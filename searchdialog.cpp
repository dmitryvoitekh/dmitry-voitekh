#include "searchdialog.h"
#include "ui_searchdialog.h"
#include <QMessageBox>


SearchDialog::SearchDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SearchDialog)
{
    ui->setupUi(this);
    ch1=ch2=ch3=ch4=false;
    for(int i=0; i<NUM; i++)
    ui->comboBox1->addItem(kind[i]);
    for(int i=0; i<NUM1; i++)
    ui->comboBox2->addItem(places_list[i]);
    ui->lineEdit->setVisible(false);
    ui->comboBox1->setVisible(false);
    ui->comboBox2->setVisible(false);
    ui->lineEdit2->setVisible(false);
    ui->lineEdit3->setVisible(false);
}

void SearchDialog::set_data(account & a)
{
    acc=a;
}

SearchDialog::~SearchDialog()
{
    delete ui;
}


void SearchDialog::on_checkBox3_clicked(bool checked)
{
    ch3=checked;
    if (ch3) ui->comboBox2->setVisible(true);
    else ui->comboBox2->setVisible(false);
}

void SearchDialog::on_checkBox1_clicked(bool checked)
{
    ch1=checked;
    if (ch1) ui->lineEdit->setVisible(true);
    else ui->lineEdit->setVisible(false);
}

void SearchDialog::on_checkBox2_clicked(bool checked)
{
    ch2=checked;
    if (ch2) ui->comboBox1->setVisible(true);
    else ui->comboBox1->setVisible(false);
}

void SearchDialog::on_pushButton_clicked()
{
    my_search.reset(acc);
    QString name=ui->lineEdit->text();
    int c=ui->comboBox1->currentIndex();
    int pl=ui->comboBox2->currentIndex();
    QTime t1=QTime::fromString(ui->lineEdit2->text());
    QTime t2=QTime::fromString(ui->lineEdit3->text());
    if (ch1) my_search.search_name(name);
    if (ch2) my_search.search_kind(c);
    if (ch3) my_search.search_place(pl);
    if (ch4) my_search.search_time(t1,t2);
    if (my_search.size())
    {
    myModel=new MyModel(this);
    myModel->set_data(my_search.form_result(),my_search.size()+1,8);
    tableView.setModel( myModel );
    tableView.resize(850, 300);
    tableView.show();
    }
    else
    QMessageBox::information(this,"Results", "Nothing appropriate was found");
    my_search.reset(acc);
    ui->comboBox1->setCurrentIndex(0);
    ui->comboBox2->setCurrentIndex(0);
    ui->lineEdit->clear();
    ui->lineEdit2->clear();
    ui->lineEdit3->clear();
}

void SearchDialog::on_checkBox4_clicked(bool checked)
{
    ch4=checked;
    if (ch4) {ui->lineEdit2->setVisible(true); ui->lineEdit3->setVisible(true);}
    else {ui->lineEdit2->setVisible(false); ui->lineEdit3->setVisible(false);}
}