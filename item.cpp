#include "item.h"
#include "place.h"
#include <vector>
#include <iostream>
#include <iomanip>
#include <QString>
#include <QDateTime>

using namespace std;

void show_categ()
{
    cout << endl;
    cout << "-------------------------------------------\n";
    for(int i=0; i<NUM/2; i++)
    //cout << i+1 << "." << setw(11)<< kind[i] << "  " << NUM/2+i+1 << "." << setw(10) << kind[NUM/2+i] << endl;
    cout << "-------------------------------------------\n";
}

item::item():sum_used(0)
{
    sign='-';
    object="None";
    category=0;
    person="Unknown";
}

item::item(int cat, const QString & o, const balance & s, const QString & per, const place & p, const QDateTime & t_d):sum_used(s),
    object(o), person(per), point(p), t(t_d)
{
    sign='-';
    if (cat<0||cat>=NUM)
    category=0;
    else
    category=cat;
}

item::item(const item & i):sum_used(i.sum_used)
{
    sign=i.sign;
    object=i.object;
    category=i.category;
    person=i.person;
    point=i.point;
    t=i.t;
}

item & item::operator=(const item & i)
{
    if(this==&i)
    return * this;
    sign=i.sign;
    sum_used=i.sum_used;
    object=i.object;
    category=i.category;
    person=i.person;
    point=i.point;
    t=i.t;
    return * this;
}


void item::set_sign(char ch)
{
    sign=ch;
}

void item::set_kind(int k)
{
    category=k;
}

void item::set_item_currency(int k)
{
    sum_used.set_currency(k);
}

void item::convert(int c)
{
    sum_used.convert(c);
}

void item::set_name(const QString &s)
{
    object=s;
}
