#ifndef SEARCH_H
#define SEARCH_H
#include <vector>
#include <iostream>
#include <item.h>
#include <account.h>
#include <string>


template<typename T>
class search
{
    private:
        std::vector<T> res;
        std::vector<T> a;
        QString ** arr;
    public:
        search();
        search(account & acc);
        void search_name(QString n);
        void search_kind(int k);
        void search_time(QTime & t1, QTime & t2);
        void search_place(int pl);
        void reset(account &acc);
        void show_result();
        int size() {return a.size();}
        QString ** form_result();
};

template<typename T>
search<T>::search()
{
}

template<typename T>
search<T>::search(account & acc)
{
    a=acc.get_list();
}

template<typename T>
void search<T>::search_name(QString n)
{
    for(int i=0; i<a.size(); i++)
    if (n==a[i]->get_name())
    res.push_back(a[i]);
    a=res;
    res.clear();
}

template<typename T>
void search<T>::search_kind(int k)
{
    for(int i=0; i<a.size(); i++)
    if (k==a[i]->get_kind())
    res.push_back(a[i]);
    a=res;
    res.clear();
}

template<typename T>
void search<T>::search_place(int pl)
{
    for(int i=0; i<a.size(); i++)
    if (pl==a[i]->get_place().get_kind())
    res.push_back(a[i]);
    a=res;
    res.clear();
}

template<typename T>
void search<T>::search_time(QTime & t1, QTime & t2)
{
    for(int i=0; i<a.size(); i++)
    if ((a[i]->get_time().time()>=t1)&&(a[i]->get_time().time()<=t2))
    res.push_back(a[i]);
    a=res;
    res.clear();
}

template<typename T>
void search<T>::reset(account & acc)
{
    a=acc.get_list();
}

template<typename T>
void search<T>::show_result()
{
    std::cout << "Result of your search:\n";
    if (!a.size())
    std::cout << "Nothing appropriate was found\n";
    else
    for(int i=0; i<a.size(); i++)
    {
    std::cout << i+1 << ". ";
    a[i]->show();
    }
    std::cout << std::endl;
}

template<typename T>
QString ** search<T>::form_result()
{
    double s=0;
    char ch=0;

    arr=new QString*[a.size()+1];
    for(int i=0; i<a.size()+1; i++)
    arr[i]=new QString[8];

    for(int i=0; i<a.size(); i++)
    {
            arr[i][0]=QString::number(i+1);
            arr[i][1]=QChar(a[i]->get_sign())+QString::number(a[i]->get_sum())+" "+currency_kind[a[i]->get_currency()];
            arr[i][2]=kind[a[i]->get_kind()];
            arr[i][3]=a[i]->get_name();
            arr[i][4]=places_list[a[i]->get_place_kind()];
            arr[i][5]=a[i]->get_place_name();
            arr[i][6]=a[i]->get_person();
            arr[i][7]=a[i]->get_time().toString();
            if(a[i]->get_sign()=='+')
                s+=a[i]->get_sum();
            else
                s-=a[i]->get_sum();
    }
    if (s<0) {s*=-1; ch='-';}
    else {ch='+';}
    arr[a.size()][0]="Total:";
    arr[a.size()][1]=QChar(ch)+QString::number(s)+" "+currency_kind[a[0]->get_currency()];

    return arr;
}

#endif // SEARCH_H