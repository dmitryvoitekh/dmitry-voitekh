#include "account.h"
#include <iostream>
#include <vector>
#include <iomanip>
#include "special_item.h"
#include "QTime"

using namespace std;

account::account()
{
    actions=0;
}

account::account(const QString & n, const balance & b): name(n), sum(b), actions(0)
{
}

account::account(const account & a):sum(a.sum)
{
    actions=a.actions;
    list=a.list;
}

account & account::operator=(const account & a)
{
    if (this==&a)
    return *this;
    actions=a.actions;
    sum=a.sum;
    list=a.list;
    return *this;
}

account::~account()
{
        list.clear();
        for(int i=0; i<actions; i++)
        delete [] arr[i];
        delete [] arr;
}

void account::spend(item & i)
{
    if (i.get_sum()>sum.get_total())
    {
        cout << "transaction aborted. not enough money on your account.\n";
    }
    else if(i.get_sum()<=0)
    {
        cout << "transaction aborted. amount of spendings must be positive.\n";
    }
    else
    {
        i.set_item_currency(sum.get_currency());
        actions++;
        sum-=i.get_sum();
        i.set_sign('-');
        list.push_back(& i);
    }
}

void account::payment(special_item & i)
{
        i.set_item_currency(sum.get_currency());
        actions++;
        sum-=i.get_sum();
        list.push_back(& i);
}

void account::earn(item & i)
{
        if(i.get_sum()<=0)
        {
            cout << "transaction aborted. amount of earnings must be positive.\n";
        }

        else
        {
            i.set_item_currency(sum.get_currency());
            actions++;
            sum+=i.get_sum();
            i.set_sign('+');
            list.push_back(& i);
        }
}

void account::reset(const balance & s)
{
    sum=s;
    actions=0;
    list.clear();
}

void account::convert_list()
{
    for(int i=0; i<actions; i++)
    list[i]->convert(get_account_currency());
}


QString ** account::list_to_string()
{
    double s=0;
    char ch=0;

    arr=new QString*[actions+1];
    for(int i=0; i<actions+1; i++)
        arr[i]=new QString[8];

    for(int i=0; i<actions; i++)
    {
            arr[i][0]=QString::number(i+1);
            arr[i][1]=QChar(list[i]->get_sign())+QString::number(list[i]->get_sum())+" "+currency_kind[list[i]->get_currency()];
            arr[i][2]=kind[list[i]->get_kind()];
            arr[i][3]=list[i]->get_name();
            arr[i][4]=places_list[list[i]->get_place_kind()];
            arr[i][5]=list[i]->get_place_name();
            arr[i][6]=list[i]->get_person();
            arr[i][7]=list[i]->get_time().toString();
            if(list[i]->get_sign()=='+')
                s+=list[i]->get_sum();
            else
                s-=list[i]->get_sum();
    }
    if (s<0) {s*=-1; ch='-';}
    else {ch='+';}
    arr[actions][0]="Total:";
    arr[actions][1]=QChar(ch)+QString::number(s)+" "+currency_kind[get_account_currency()];
    arr[actions][2]="Left:";
    arr[actions][3]=QString::number(sum.get_total())+" "+currency_kind[get_account_currency()];
    return arr;
}

