#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets/QTableView>
#include "mymodel.h"
#include <list>
#include "item.h"
#include "account.h"
#include "searchdialog.h"
#include "diagram.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushItem_clicked();

    void on_actionInformation_triggered();

    void on_actionChange_currency_triggered();

    void on_actionReset_data_triggered();

    void on_actionTable_triggered();

    void on_actionSearch_triggered();

    void on_actionDiagram_triggered();

private:
    Ui::MainWindow *ui;
    QTableView tableView;
    MyModel * myModel;
    SearchDialog * mySearch;
    diagram * diag;

    std::list<item> l_it;
    std::list<special_item> l_sit;
    account a;
    int count;
    double * arr;
};

#endif // MAINWINDOW_H