#ifndef SEARCHDIALOG_H
#define SEARCHDIALOG_H

#include <QDialog>
#include <QtWidgets/QTableView>
#include "search.h"
#include "item.h"
#include "mymodel.h"
#include "account.h"

namespace Ui {
class SearchDialog;
}

class SearchDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SearchDialog(QWidget *parent = 0);
    ~SearchDialog();
    void set_data(account & a);

private slots:

    void on_checkBox3_clicked(bool checked);

    void on_checkBox1_clicked(bool checked);

    void on_checkBox2_clicked(bool checked);

    void on_pushButton_clicked();

    void on_checkBox4_clicked(bool checked);

private:
    Ui::SearchDialog *ui;
    search<item *> my_search;
    QTableView tableView;
    MyModel * myModel;
    account acc;
    bool ch1,ch2,ch3,ch4;
};

#endif // SEARCHDIALOG_H