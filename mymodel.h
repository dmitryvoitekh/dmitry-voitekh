#ifndef MODEL_H
#define MODEL_H
// mymodel.h
#include <QAbstractTableModel>
#include <QString>

class MyModel : public QAbstractTableModel
{
    Q_OBJECT
private:
    int row;
    int col;
    QString ** array;
public:
    MyModel(QObject *parent=0);
    void set_data(QString ** arr, int y, int x);
    int rowCount(const QModelIndex &parent = QModelIndex()) const ;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
};
#endif // MODEL_H
