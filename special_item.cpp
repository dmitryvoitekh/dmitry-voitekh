#include "special_item.h"
#include "item.h"
#include "balance.h"
#include "QTime"
#include <iostream>

using namespace std;

special_item::special_item(int c):item()
{
    set_name(pay_kind[c]);
    set_kind(NUM-1);
    QTime midnight(0,0,0);
    qsrand(midnight.secsTo(QTime::currentTime()));
    set_sum((double)(qrand() % 70 + 7)/7);
}

void special_item::operator=(item & i)
{
    set_place(i.get_place());
    set_person(i.get_person());
    set_time(i.get_time());
}
