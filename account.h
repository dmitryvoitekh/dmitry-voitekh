#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <string>
#include <iostream>
#include <vector>
#include "item.h"
#include "balance.h"
#include "special_item.h"
#include <QString>

class account
{
    private:
        balance sum;
        QString name;
        std::vector<item *> list;
        int actions;
        QString ** arr;
    public:
        account();
        account(const QString & n, const balance & b);
        account(const account & a);
        ~account();
        account & operator=(const account & a);
        void spend(item & i);
        void earn(item & i);
        void payment(special_item & i);
        void reset(const balance & s);
        int get_account_currency() {return sum.get_currency();}
        double get_account_balance() {return sum.get_total();}
        int get_actions() {return actions;}
        QString & get_account_name() {return name;}
        void set_account_name(const QString & n) {name=n;}
        std::vector<item *> & get_list() {return list;}
        void input_account_currency(int c) {sum.input_currency(c);}
        item * operator[](int i){return list[i];}
        QString ** list_to_string();
        void convert_list();
};

#endif // ACCOUNT_H
