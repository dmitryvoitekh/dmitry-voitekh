#ifndef ITEM_H
#define ITEM_H

#include <string>
#include <iostream>
#include <vector>
#include "place.h"
#include "balance.h"
#include <QString>
#include <QDateTime>

const int NUM=10;
const QString kind[NUM]={"Other", "Sport", "Electronics", "Property", "Food", "Toys", "Clothes", "Stationary", "Tools", "Daily payment"};
void show_categ();

class item
{
private:
    balance sum_used;
    char sign;
    int category;
    QString object;
    QString person;
    place point;
    QDateTime t;
public:
    item();
    item(int cat, const QString & o, const balance & s, const QString & per, const place & p, const QDateTime & t_d);
    item(const item & i);
    item & operator=(const item & i);
    char get_sign() {return sign;}
    double get_sum() {return sum_used.get_total();}
    const QString & get_name(){return object;}
    int get_kind(){return category;}
    int get_currency() {return sum_used.get_currency();}
    place & get_place(){return point;}
    int get_place_kind(){return point.get_kind();}
    QString & get_place_name(){return point.get_name();}
    QString & get_person(){return person;}
    QDateTime & get_time() {return t;}
    void set_sum(double b) {sum_used=b;}
    void set_name(const QString &s);
    void set_place(const place & p) {point=p;}
    void set_person(const QString & p) {person=p;}
    void set_sign(char ch='+');
    void set_item_currency(int k);
    void set_kind(int k);
    void set_time(QDateTime tim) {t=tim;}
    void convert(int c);
};

#endif // ITEM_H