// mymodel.cpp
#include "mymodel.h"

MyModel::MyModel(QObject *parent)
    :QAbstractTableModel(parent)
{
    row=col=0;
}


void MyModel::set_data(QString ** arr, int y, int x)
{
    array=arr;
    col=x;
    row=y;
}

int MyModel::rowCount(const QModelIndex & parent) const
{
   return row;
}

int MyModel::columnCount(const QModelIndex & parent) const
{
    return col;
}

QVariant MyModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
    {
       return array[index.row()][index.column()];
    }
    return QVariant();
}

QVariant MyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal) {
            switch (section)
            {
            case 0:
                return QString("№");
            case 1:
                return QString("Sum");
            case 2:
                return QString("Category");
            case 3:
                return QString("Object name");
            case 4:
                return QString("Place category");
            case 5:
                return QString("Place name");
            case 6:
                return QString("Person");
            case 7:
                return QString("Time");
            }
        }
    }
    return QVariant();
}
