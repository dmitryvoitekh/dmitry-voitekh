#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
#include <QtCore>
#include <QtGui>
#include <QInputDialog>
#include <QMessageBox>
#include <QtWidgets/QApplication>
#include <QtWidgets/QTableView>
#include <QDateTime>
#include <QTime>
#include "item.h"
#include "place.h"

using namespace std;
#define TURN 2

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    count=0;
    QStringList cur;
    for(int i=0; i<NUM2; i++)
    cur << currency_kind[i];
    for(int i=0; i<NUM; i++)
    ui->comboBox1->addItem(kind[i]);
    for(int i=0; i<NUM1; i++)
    ui->comboBox2->addItem(places_list[i]);
    ui->comboBox->addItem("Spend");
    ui->comboBox->addItem("Earn");
        QString n = QInputDialog::getText(this, tr("Start with new account"),
                                             tr("User name:"), QLineEdit::Normal,
                                             QDir::home().dirName());
        QString k=QInputDialog::getItem(this, tr("Start with new account"),tr("Choose a currency:"),cur,0,false);
        double sum = QInputDialog::getDouble(this, tr("Start with new account"),
                                              tr("Balance:"), 2000.00, 0, 10000, 1);
        int j;
        for(j=0; currency_kind[j]!=k; j++);
        balance b(sum,j);
        a=account(n,b); a.set_account_name(n);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushItem_clicked()
{
    int cat1 = ui->comboBox1->currentIndex();
    int cat2 = ui->comboBox2->currentIndex();
    QString name=ui->lineEdit2->text();
    QString sum=ui->lineEdit1->text();
    QString pl=ui->lineEdit3->text();
    QString per=ui->lineEdit4->text();
    place p(pl,cat2);
    QDateTime dateTime = QDateTime::currentDateTime();
    item i(cat1,name,sum.toDouble(),per,p,dateTime);
    l_it.push_back(i);
    switch(ui->comboBox->currentIndex())
    {
    case 0:
        a.spend(l_it.back());
        break;
    case 1:
        a.earn(l_it.back());
        break;
    }
    count++;
    if(count==TURN)
    {
        QTime midnight(0,0,0);
        qsrand(midnight.secsTo(QTime::currentTime()));
        special_item is(qrand()%NUM3);
        is=l_it.back();
        l_sit.push_back(is);
        a.payment(l_sit.back());
        QMessageBox::information(this,"Information","Regular payment:\n-"+QString::number(is.get_sum())+" "+
                                 currency_kind[is.get_currency()]+", "+is.get_name());
        count=0;
    }
    ui->comboBox->setCurrentIndex(0);
    ui->comboBox1->setCurrentIndex(0);
    ui->comboBox2->setCurrentIndex(0);
    ui->comboBox->setCurrentIndex(0);
    ui->lineEdit1->clear();
    ui->lineEdit2->clear();
    ui->lineEdit3->clear();
    ui->lineEdit4->clear();
}

void MainWindow::on_actionInformation_triggered()
{
    QMessageBox::information(this,"Information","name: "+a.get_account_name()+
                             "\ntotal: "+QString::number(a.get_account_balance())+
                             " "+currency_kind[a.get_account_currency()]);
}

void MainWindow::on_actionChange_currency_triggered()
{
    QStringList cur;
    for(int i=0; i<NUM2; i++)
    cur << currency_kind[i];
    QString k=QInputDialog::getItem(this, tr("Changing currency"),tr("Choose a new currency:"),cur,0,false);
    int j;
    for(j=0; currency_kind[j]!=k; j++);
    a.input_account_currency(j);
    a.convert_list();
    QMessageBox::information(this,"Succeded","Now your balance is:\n"+QString::number(a.get_account_balance())+
                             " "+currency_kind[a.get_account_currency()]);
}

void MainWindow::on_actionReset_data_triggered()
{
    QStringList cur;
    for(int i=0; i<NUM2; i++)
    cur << currency_kind[i];
    QString n = QInputDialog::getText(this, tr("Resetting"),
                                         tr("User name:"), QLineEdit::Normal,
                                         QDir::home().dirName());
    QString k=QInputDialog::getItem(this, tr("Resetting"),tr("Choose a currency:"),cur,0,false);
    double sum = QInputDialog::getDouble(this, tr("Resetting"),
                                          tr("Balance:"), 2000.00, 0, 10000, 1);
    int j;
    for(j=0; currency_kind[j]!=k; j++);
    balance b(sum,j);
    a.set_account_name(n);
    a.reset(b);
    QMessageBox::information(this,"Succeded","Your account has been reset\nname: "+
                             a.get_account_name()+"\ntotal: "+
                             QString::number(a.get_account_balance())+" "+
                             currency_kind[a.get_account_currency()]);
}

void MainWindow::on_actionTable_triggered()
{
    myModel=new MyModel(this);
    myModel->set_data(a.list_to_string(),a.get_actions()+1,8);
    tableView.setModel( myModel );
    tableView.resize(850, 300);
    tableView.show();
}

void MainWindow::on_actionSearch_triggered()
{
    mySearch=new SearchDialog(this);
    mySearch->set_data(a);
    mySearch->show();
}

void MainWindow::on_actionDiagram_triggered()
{
    diag=new diagram(this);
    QStringList pla;
    for(int i=0; i<NUM1; i++)
    pla << places_list[i];
    QString pl=QInputDialog::getItem(this, tr("Constructing diagram"),
                                     tr("Choose a place category:"),pla,0,false);
    int j=0; int c=0;
    for(j=0; places_list[j]!=pl; j++);
    arr=new double[a.get_actions()];
    for(int i=0;i<a.get_actions();i++)
        if (a[i]->get_place_kind()==j) arr[c++]=a[i]->get_sum();
    if(c)
    {
    diag->set_data(arr,c);
    diag->show();
    }
    else
    QMessageBox::information(this,"Failed","No data for diagram constructing");
}