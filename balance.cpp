#include "balance.h"
#include<iostream>
#include <iomanip>
using namespace std;

void show_currencies()
{
    cout << endl;
    for(int i=0; i<NUM2; i++)
    //cout << i+1 << "." << currency_kind[i] << endl;
    cout << endl;
}

balance::balance()
{
    currency=0;
    total=0;
}

balance::balance(double t, int c):total(t)
{
    if (c<0||c>=NUM2)
    currency=0;
    else
    currency=c;
}

void balance::operator+=(double s)
{
    total+=s;
}

void balance::operator-=(double s)
{
    total-=s;
}

void balance::set_currency(int k)
{
    currency=k;
}

void balance::input_currency(int c)
{
    total*=rate[c]/rate[currency];
    *this=balance(total,c);
}

void balance::convert(int c)
{
    total*=rate[c]/rate[currency];
    currency=c;
}