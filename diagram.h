#include "diagram.h"
#include "ui_diagram.h"

diagram::diagram(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::diagram)
{
    ui->setupUi(this);
}

diagram::~diagram()
{
    delete ui;
}

void diagram::set_data(double * a, int n)
{
    arr=a;
    NUM=n;
}

void diagram::paintEvent(QPaintEvent *e)
{
    QPainter pai(this);
    QRect *rec;
    int indent=15;
    int s=indent;
    int c=0;
    int height=300-indent;
    int width=400-indent;
    int max=arr[0];
    for(int i=0; i<NUM; i++)
        if(max<arr[i]) max=arr[i];
    int side=width/NUM;
    if (side>80) side=80;
    QBrush * brush;
    for(int i=0; i<NUM; i++)
    {
        switch (c)
        {
        case 0:
            brush=new QBrush(Qt::blue);
            pai.setBrush(*brush);
            break;
        case 1:
            brush=new QBrush(Qt::red);
            pai.setBrush(*brush);
            break;
        case 2:
            brush=new QBrush(Qt::green);
            pai.setBrush(*brush);
            break;
        case 3:
            brush=new QBrush(Qt::cyan);
            pai.setBrush(*brush);
            break;
        case 4:
            brush=new QBrush(Qt::yellow);
            pai.setBrush(*brush);
            break;
        case 5:
            brush=new QBrush(Qt::gray);
            pai.setBrush(*brush);
            break;
        default:
            break;
        }
        QPen mypen(Qt::black);
        mypen.setWidth(2);
        pai.setPen(mypen);
        rec=new QRect(s,height-arr[i]*height/max,side,arr[i]*height/max);
        pai.drawRect(*rec);
        c++;
        if(c>5) c=0;
        s+=side;
    }
pai.drawLine(indent,indent,indent,height-indent);
pai.drawLine(indent,height,width,height);
}