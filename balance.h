#ifndef BALANCE_H
#define BALANCE_H
#include<iostream>
#include <QString>

const int NUM2=5;
const QString currency_kind[NUM2]={"$USA", "hrn.UA", "euro", "rub.RUS", "pnd.UK"};
const double rate[5]={1, 13.12, 0.79, 39.52, 0.61};

class balance
{
private:
    double total;
    int currency;
public:
    balance();
    balance(double t, int c=0);
    void set_currency(int k);
    void input_currency(int c);
    double get_total() {return total;}
    double get_currency() {return currency;}
    void operator+=(double s);
    void operator-=(double s);
    void convert(int c);
    friend void show_currencies();
};

#endif // BALANCE_H

