#ifndef PLACE_H
#define PLACE_H
#include <iostream>
#include <QString>

const int NUM1=10;

const QString places_list[NUM1]={"Undefined place", "Department store", "Online shop", "Supermarket", "Sports store", "Grocery", "Clothing store", "Outlet", "Restaurant", "Saloon"};
void show_places();

class place
{
    private:
        QString name;
        int kind;
    public:
        place();
        place(QString n, int k=0);
        ~place();
        void reset(QString n="Unknown", int k=0);
        int get_kind() {return kind;}
        QString & get_name() {return name;}
        friend std::ostream & operator<<(std::ostream & os, const place & p);
};

#endif // PLACE_H