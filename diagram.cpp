#ifndef DIAGRAM_H
#define DIAGRAM_H

#include <QDialog>
#include <QtCore>
#include <QtGui>
#include "account.h"
#include "item.h"

namespace Ui {
class diagram;
}

class diagram : public QDialog
{
    Q_OBJECT

public:
    explicit diagram(QWidget *parent = 0);
    ~diagram();
    void set_data(double * a, int n);

private:
    Ui::diagram *ui;
    void paintEvent(QPaintEvent *e);
    double * arr;
    int NUM;
};

#endif // DIAGRAM_H